################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
inc/interrupt.obj: ../inc/interrupt.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"/opt/ti/ccsv6/tools/compiler/c6000_7.4.18/bin/cl6x" -mv6740 -g --include_path="/opt/ti/ccsv6/tools/compiler/c6000_7.4.18/include" --include_path="/home/delvis/workspace/ccsv6/all_test/inc" --diag_warning=225 --abi=coffabi --preproc_with_compile --preproc_dependency="inc/interrupt.d" --obj_directory="inc" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: $<'
	@echo ' '

inc/intvecs.obj: ../inc/intvecs.asm $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"/opt/ti/ccsv6/tools/compiler/c6000_7.4.18/bin/cl6x" -mv6740 -g --include_path="/opt/ti/ccsv6/tools/compiler/c6000_7.4.18/include" --include_path="/home/delvis/workspace/ccsv6/all_test/inc" --diag_warning=225 --abi=coffabi --preproc_with_compile --preproc_dependency="inc/intvecs.d" --obj_directory="inc" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: $<'
	@echo ' '


